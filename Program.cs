﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NADAImport
{
    class Program
    {
        static void Main(string[] args)
        {
            using (TIVDATAEntities db = new TIVDATAEntities())
            {
                string importDirectory = System.Configuration.ConfigurationManager.AppSettings["import.directory"];

                Console.WriteLine("clearing inactive data");
                db.spNadaClearInactive();

                Console.WriteLine("reading zip files from {0}", importDirectory);
                foreach (string fileName in Directory.GetFiles(importDirectory, "*.zip"))
                {
                    Console.WriteLine("processing {0}", fileName);

                    Dictionary<string, string> zips = UnZipToMemory(fileName);
                    foreach (string zipName in zips.Keys)
                    {
                        if (zipName.ToLower().Contains("modeldetailsp"))
                        {
                            //this is a model detail file
                            List<string> fnameparts = zipName.ToLower().Trim().Split(new char[] { '_' }).ToList();
                            string source = fnameparts[1];

                            Console.WriteLine("processing \"model details\" source \"{1}\" file {0}", zipName, source);

                            PipeDelimitedFile pdf = new PipeDelimitedFile(zips[zipName]);

                            for (int i = 0; i < pdf.RowCount; i++)
                            {
                                NADA_ModelDetailsMaster detail = new NADA_ModelDetailsMaster();

                                detail.DateEntered = DateTime.Now;
                                detail.Active = false;
                                detail.Version = pdf.GetValue<double>(i, "version");
                                detail.FileSource = source;

                                string make = pdf.GetValue<string>(i, "company");
                                make = make.Replace(" TRUCK", "");
                                make = make.Replace(" LIGHT DUTY", "");
                                make = make.Trim();

                                detail.Company = make;
                                detail.CompanyNum = pdf.GetValue<double>(i, "companynum");
                                detail.ModelYear = pdf.GetValue<int>(i, "modelyear");
                                detail.ModelCat = pdf.GetValue<string>(i, "modelcat").Trim();
                                detail.ModelNum = pdf.GetValue<int>(i, "modelnum");
                                detail.Model = pdf.GetValue<string>(i, "model").Trim();
                                detail.OptionKey = pdf.GetValue<long?>(i, "optionkey");
                                detail.RoughTradeIn = pdf.GetValue<int>(i, "roughtradein");
                                detail.AverageTradeIn = pdf.GetValue<int>(i, "averagetradein");
                                detail.CleanTradeIn = pdf.GetValue<int>(i, "cleantradein");
                                detail.CleanRetail = pdf.GetValue<int>(i, "cleanretail");
                                detail.MileageClass = pdf.GetValue<int>(i, "mileageclass");

                                db.NADA_ModelDetailsMaster.AddObject(detail);
                            }

                            db.SaveChanges();
                        }
                        else if (zipName.ToLower().Contains("mileageadjustment"))
                        {
                            //this is a mileage adjustments file
                            Console.WriteLine("processing \"mileage adjustments\" file {0}", zipName);

                            PipeDelimitedFile pdf = new PipeDelimitedFile(zips[zipName]);

                            for (int i = 0; i < pdf.RowCount; i++)
                            {
                                NADA_MileageAdjustmentsMaster madj = new NADA_MileageAdjustmentsMaster();

                                madj.Active = false;
                                madj.DateEntered = DateTime.Now;
                                madj.Version = pdf.GetValue<double>(i, "version");

                                madj.MileageClass = pdf.GetValue<int>(i, "mileageclass");
                                madj.ModelYear = pdf.GetValue<int>(i, "yearkey");
                                madj.StartMiles = pdf.GetValue<int>(i, "startmiles");
                                madj.EndMiles = pdf.GetValue<int?>(i, "endmiles");
                                madj.MaxAdjPct = pdf.GetValue<double?>(i, "maxadjpct");
                                madj.Factor = pdf.GetValue<double>(i, "factor");

                                madj.Notes = pdf.GetValue<string>(i, "notes");
                                madj.IsPct = pdf.GetValue<bool>(i, "ispct");

                                db.NADA_MileageAdjustmentsMaster.AddObject(madj);
                            }

                            db.SaveChanges();
                        }
                        else if (zipName.ToLower().Contains("optionp") || zipName.ToLower().Contains("usedcaroptions"))
                        {
                            //List<string> fnameparts = zipName.ToLower().Trim().Split(new char[] { '_' }).ToList();
                            string source = "ALL";

                            Console.WriteLine("processing \"options\" source \"{1}\" file {0}", zipName, source);

                            PipeDelimitedFile pdf = new PipeDelimitedFile(zips[zipName]);

                            for (int i = 0; i < pdf.RowCount; i++)
                            {
                                NADA_OptionsMaster madj = new NADA_OptionsMaster();

                                madj.Active = false;
                                madj.DateEntered = DateTime.Now;
                                madj.Version = pdf.GetValue<double>(i, "versionid");
                                madj.FileSource = source;

                                if (
                                    pdf.GetValue<string>(i, "optionkey") != null
                                    &&
                                    pdf.GetValue<string>(i, "optionnum") != null
                                    )
                                {
                                    //madj.CompanyNum = pdf.GetValue<double>(i, "companynum");
                                    madj.OptionKey = pdf.GetValue<long>(i, "optionkey");
                                    madj.OptionNum = pdf.GetValue<long>(i, "optionnum");
                                }

                                madj.Description = pdf.GetValue<string>(i, "description");

                                madj.RoughTradein = pdf.GetValue<int>(i, "roughtradein");
                                madj.AverageTradein = pdf.GetValue<int>(i, "averagetradein");
                                madj.CleanTradein = pdf.GetValue<int>(i, "cleantradein");
                                madj.CleanRetail = pdf.GetValue<int>(i, "cleanretail");

                                //madj.ModelYear = pdf.GetValue<int>(i, "modelyear");
                                madj.ModelNum = pdf.GetValue<int>(i, "modelnum");

                                //madj.ValuePercent = pdf.GetValue<double>(i, "valuepercent");

                                db.NADA_OptionsMaster.AddObject(madj);
                            }

                            db.SaveChanges();
                        }
                        else if (zipName.ToLower().Contains("companies"))
                        {
                            //this is a model detail file
                            List<string> fnameparts = zipName.ToLower().Trim().Split(new char[] { '_' }).ToList();
                            string source = fnameparts[1];

                            Console.WriteLine("processing \"companies\" source \"{1}\" file {0}", zipName, source);

                            PipeDelimitedFile pdf = new PipeDelimitedFile(zips[zipName]);

                            for (int i = 0; i < pdf.RowCount; i++)
                            {
                                NADA_CompaniesMaster detail = new NADA_CompaniesMaster();

                                string make = pdf.GetValue<string>(i, "company");
                                make = make.Replace(" TRUCK", "");
                                make = make.Replace(" LIGHT DUTY", "");
                                make = make.Trim();

                                detail.DateEntered = DateTime.Now;
                                detail.Active = false;
                                detail.Version = pdf.GetValue<double>(i, "version");
                                detail.FileSource = source;

                                detail.CompanyNum = pdf.GetValue<double>(i, "companynum");
                                detail.Company = make;

                                db.NADA_CompaniesMaster.AddObject(detail);
                            }

                            db.SaveChanges();
                        }
                    }
                }

                Console.WriteLine("making new data active");
                db.spNadaMoveInactiveToActive();
            }
        }

        private static Dictionary<string, string> UnZipToMemory(string fileName)
        {
            var result = new Dictionary<string, string>();
            using (ZipFile zip = ZipFile.Read(fileName))
            {
                foreach (ZipEntry e in zip)
                {
                    using (MemoryStream data = new MemoryStream())
                    {
                        e.Extract(data);
                        string sData = System.Text.Encoding.Default.GetString(data.ToArray());
                        result.Add(e.FileName, sData);
                    }
                }
            }
            return result;
        }

        private class PipeDelimitedFile
        {
            private Dictionary<string, int> _fieldNames = new Dictionary<string, int>();
            private List<List<string>> _fieldData = new List<List<string>>();

            public PipeDelimitedFile(string rawData)
            {
                using (StringReader reader = new StringReader(rawData))
                {
                    //first line is field definition
                    List<string> lineData = parse(reader.ReadLine());
                    for (int i = 0; i < lineData.Count(); i++)
                    {
                        _fieldNames.Add(lineData[i].Trim().ToLower(), i);
                    }

                    //now do each new line
                    string rawLine = reader.ReadLine();
                    while (rawLine != null)
                    {
                        _fieldData.Add(parse(rawLine));
                        rawLine = reader.ReadLine();
                    }
                }
            }

            public int RowCount
            {
                get
                {
                    return _fieldData.Count();
                }
            }
            public int FildCount
            {
                get
                {
                    return _fieldNames.Count();
                }
            }

            public T GetValue<T>(int rowNumber, string fieldName)
            {

                string val = _fieldData[rowNumber][_fieldNames[fieldName.Trim().ToLower()]];
                if (val == "NULL") val = null;

                Type t = typeof(T);
                Type u = Nullable.GetUnderlyingType(t);

                if (u == null)
                {
                    return (T)Convert.ChangeType(val, t);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(val))
                        return default(T);
                    return (T)Convert.ChangeType(val, u);
                }

            }

            private List<string> parse(string lineData)
            {
                return lineData.Trim().Split(new char[] { '|' }, StringSplitOptions.None).ToList();
            }
        }


    }
}
